![banerGit](https://user-images.githubusercontent.com/16636086/106938115-ded34680-671e-11eb-8de4-35fd6d00868a.png)
# Spring Boot JWT Authentication example with Spring Security & Spring Data JPA + Oracle 19c as DB.

## Configure Spring Datasource, JPA, App properties
Open `src/main/resources/application.properties`
- For Oracle 19c:
```
#Oracle database
spring.datasource.url=jdbc:oracle:thin:@localhost:1521:orcl
spring.datasource.username=db_username
spring.datasource.password=db_password
spring.datasource.driver-class-name=oracle.jdbc.driver.OracleDriver

#JPA Configurations
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.Oracle10gDialect
spring.jpa.open-in-view=true
spring.jpa.show-sql=true
spring.jpa.hibernate.ddl-auto=update

#Web server port
server.port=80

# App Properties
echo.app.jwtSecret=echoSecretKey
echo.app.jwtExpirationMs=86400000
```
## Run Spring Boot application
```
mvn spring-boot:run
```

## Run following SQL insert statements
Open `src/main/resources/database-creation-script`
Execute `00_install.sql`
```
You need to give moderator or admin role manually in the DB using sqlplus  or  sqldeveloper.
User and password by default : admin/password | user/password .
```

## END POINTS!
signin `localhost/api/auth/signin`
```
{
    "username": "admin",
    "password": "password"
}
```
signup `localhost/api/auth/signup`
```
{
    "username": "username",
    "password": "password"
}
```
Public test `localhost/api/test/all`
```
Public Content.
```
User test `localhost/api/test/user`
```
User Content.
```
Moderator test `localhost/api/test/mod`
```
Moderator board.
```
Admin test `localhost/api/test/admin`
```
Admin Board.
```
Users `localhost/users`

<details><summary>Return all users JSON.</summary>
[
    {
        "user_idpk": 1,
        "username": "user",
        "password": "$2a$12$SqdqykVgTJjFh7Nw0lIWVeLShhynqAAYmEmB/3IpliqPnznFsf9bi",
        "email": null,
        "strikes": 0,
        "enabled": 1,
        "user_roles": [
            {
                "role_idpk": 1,
                "role_name": "ROLE_USER"
            }
        ]
    },
    ....
]
</details>


