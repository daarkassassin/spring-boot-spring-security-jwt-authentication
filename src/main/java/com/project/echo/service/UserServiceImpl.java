package com.project.echo.service;

import static java.util.Collections.emptyList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.echo.dao.IUser_dataDAO;
import com.project.echo.dto.User_data;
import com.project.echo.service.Iservice.IUserServiceImpl;



@Service
public class UserServiceImpl implements IUserServiceImpl {
	@Autowired
	private IUser_dataDAO iUserDAO;
	
	@Override
	public List<User_data> listUsers() {
		return iUserDAO.findAll();
	}

	@Override
	public User_data saveUser(User_data user_data) {
		return iUserDAO.save(user_data);
	}
	
	@Override
	public User_data userByUsername(String username) {
		return iUserDAO.findByUsername(username);
	}

	@Override
	public User_data userByUserid(long user_id) {
		return iUserDAO.findById(user_id).get();
	}

	@Override
	public User_data updateUser(User_data user_data) {
		return iUserDAO.save(user_data);
	}

	@Override
	public void deleteUser(long user_id) {
		iUserDAO.deleteById(user_id);
		
	}
}
