package com.project.echo.service.Iservice;

import java.util.List;
import com.project.echo.dto.User_data;

public interface IUserServiceImpl { 
	
	public List<User_data> listUsers();
	
	public User_data saveUser(User_data user_data);
	
	public User_data userByUsername(String username);
	
	public User_data userByUserid(long user_id);
	
	public User_data updateUser(User_data user_data);
	
	public void deleteUser(long user_id);

}
