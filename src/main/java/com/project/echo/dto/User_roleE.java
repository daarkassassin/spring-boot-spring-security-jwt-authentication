package com.project.echo.dto;

public enum User_roleE {
	ROLE_USER,
	ROLE_MODERATOR,
	ROLE_ADMIN
}
