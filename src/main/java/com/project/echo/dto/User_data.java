package com.project.echo.dto;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.sun.istack.NotNull;


@Entity
@Table(name = "TB_USER")
public class User_data  {
	
	@Id
	@NotNull
	@Column(name = "USER_IDPK", nullable = false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_Sequence")
	@SequenceGenerator(name = "id_Sequence", sequenceName = "SEQ_TB_USER", allocationSize=1)
	private long user_idpk;
	
	@NotNull
    @Column(name = "USERNAME", unique = true, nullable = false)
	private String username;

	@NotNull
    @Column(name = "PASSWORD", nullable = false)
	private String password;
	
	@Column(name = "EMAIL", nullable = true)
	private String email;
	
	@Column(name = "STRIKES", nullable = true)
	private Integer strikes;
	
	@Column(name = "ENABLED", nullable = true)
	private Integer enabled;
	
	//@ManyToOne
	//@JoinColumn(name = "ROLE_IDFK")
	//User_role user_role;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(	name = "IT_USER_ROLE", 
				joinColumns = @JoinColumn(name = "USER_ID"), 
				inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
	private Set<User_role> user_roles = new HashSet<>();
     
    public User_data() {
		
	}

    public User_data(String username, String password) {
		this.username = username;
		this.password = password;
	}

	/**
	 * @return the user_idpk
	 */
	public long getUser_idpk() {
		return user_idpk;
	}

	/**
	 * @param user_idpk the user_idpk to set
	 */
	public void setUser_idpk(long user_idpk) {
		this.user_idpk = user_idpk;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the strikes
	 */
	public Integer getStrikes() {
		return strikes;
	}

	/**
	 * @param strikes the strikes to set
	 */
	public void setStrikes(Integer strikes) {
		this.strikes = strikes;
	}

	/**
	 * @return the enabled
	 */
	public Integer getEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the user_roles
	 */
	public Set<User_role> getUser_roles() {
		return user_roles;
	}

	/**
	 * @param user_roles the user_roles to set
	 */
	public void setUser_roles(Set<User_role> user_roles) {
		this.user_roles = user_roles;
	}
	
	

	
}
