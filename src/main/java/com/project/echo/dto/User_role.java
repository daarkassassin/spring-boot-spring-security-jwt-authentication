package com.project.echo.dto;

import javax.persistence.*;

import com.sun.istack.NotNull;


@Entity
@Table(name = "TM_ROLE")
public class User_role  {
	
	@Id
	@NotNull
	@Column(name = "ROLE_IDPK", nullable = false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_Sequence")
	@SequenceGenerator(name = "id_Sequence", sequenceName = "SEQ_TM_ROLE", allocationSize=1)
	private long role_idpk;
	
    @Enumerated(EnumType.STRING)
	@Column(name = "ROLE_NAME", nullable = false, length = 20)
	private User_roleE rolename;

     
    public User_role() {
		
	}

    public User_role(User_roleE role_name) {
		this.rolename = role_name;
	}

	/**
	 * @return the role_idpk
	 */
	public long getRole_idpk() {
		return role_idpk;
	}

	/**
	 * @param role_idpk the role_idpk to set
	 */
	public void setRole_idpk(long role_idpk) {
		this.role_idpk = role_idpk;
	}

	/**
	 * @return the role_name
	 */
	public User_roleE getRole_name() {
		return rolename;
	}

	/**
	 * @param role_name the role_name to set
	 */
	public void setRole_name(User_roleE role_name) {
		this.rolename = role_name;
	}
	
	

  
}
