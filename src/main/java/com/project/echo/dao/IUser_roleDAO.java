package com.project.echo.dao;

import javax.persistence.Column;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.echo.dto.User_role;

@Repository
public interface IUser_roleDAO extends JpaRepository <User_role, Long>{
	
	User_role findByRolename(String role_name);

}
