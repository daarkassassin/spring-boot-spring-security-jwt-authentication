package com.project.echo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.echo.dto.User_data;

@Repository
public interface IUser_dataDAO extends JpaRepository <User_data, Long>{
	
	User_data findByUsername(String username);
	
	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);

}
