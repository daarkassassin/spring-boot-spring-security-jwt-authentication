package com.project.echo.security.Service;

import static java.util.Collections.emptyList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.echo.dto.User_data;
import com.project.echo.dao.IUser_dataDAO;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	private IUser_dataDAO userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User_data user = userRepository.findByUsername(username);
				if (user == null) {
					throw new UsernameNotFoundException("User Not Found with username: " + username);
				}
		return UserDetailsImpl.build(user);
	}

}
