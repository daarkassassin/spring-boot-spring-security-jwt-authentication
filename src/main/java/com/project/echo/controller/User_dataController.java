package com.project.echo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.project.echo.dao.IUser_dataDAO;
import com.project.echo.dto.User_data;
import com.project.echo.service.UserServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class User_dataController {
	
	@Autowired
	private UserServiceImpl userServiceImpl;
	
	private IUser_dataDAO iUser_dataDAO;

	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@PostMapping("/users")
	public void saveUsuario(@RequestBody User_data user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userServiceImpl.saveUser(user);
	}

	@GetMapping("/users")
	public List<User_data> getUsers() {
		return userServiceImpl.listUsers();
	}

	@GetMapping("/users/{username}")
	public User_data getUser(@PathVariable String username) {
		return userServiceImpl.userByUsername(username);
	}




}
