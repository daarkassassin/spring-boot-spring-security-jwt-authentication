SET DEFINE OFF;
SET SERVEROUTPUT ON;

-------------------------------------------------------------------------
-- D R O P -- I N D E X S
-------------------------------------------------------------------------
BEGIN
	EXECUTE IMMEDIATE 'DROP INDEX UN1_TB_USER';
	EXECUTE IMMEDIATE 'DROP INDEX UN2_TB_USER';
EXCEPTION
  WHEN OTHERS THEN
    IF SQLCODE != -1418 THEN
      RAISE;
    END IF;
END;
/

-------------------------------------------------------------------------
-- D R O P -- S E Q U E N C E S
-------------------------------------------------------------------------
BEGIN
    EXECUTE IMMEDIATE 'DROP SEQUENCE SEQ_TB_USER';
	EXECUTE IMMEDIATE 'DROP SEQUENCE SEQ_TM_ROLE';
	EXECUTE IMMEDIATE 'DROP SEQUENCE SEQ_TB_LOG';
EXCEPTION
  WHEN OTHERS THEN
    IF SQLCODE != -2289 THEN
      RAISE;
    END IF;
END;
/

-------------------------------------------------------------------------
-- D R O P -- T A B L E S
-------------------------------------------------------------------------
BEGIN 
    EXECUTE IMMEDIATE 'DROP TABLE IT_USER_ROLE';
    EXECUTE IMMEDIATE 'DROP TABLE TB_USER';
	EXECUTE IMMEDIATE 'DROP TABLE TM_ROLE';
	EXECUTE IMMEDIATE 'DROP TABLE TB_LOG';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/

------------------------------------------------------------------------
-- FUNCTION: FN_WHOIAM
------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION FN_WHOIAM RETURN VARCHAR2 AUTHID CURRENT_USER IS
    v_result VARCHAR2(50);
BEGIN
    EXECUTE IMMEDIATE 'select nvl(v(''APP_USER''),user) from dual' INTO v_result;
    RETURN v_result;
EXCEPTION
    WHEN OTHERS THEN
        return user;
END FN_WHOIAM;
/

-------------------------------------------------------------------------
-- CREATE -- TABLES, SEQUENCES, INDEX, TRIGGERS 
-------------------------------------------------------------------------
DECLARE
	v_column_exists NUMBER;
	v_table_exists NUMBER;
	v_seq_exists NUMBER;
	v_index_exists NUMBER;
	v_errm VARCHAR2(200);
BEGIN
    -- ----------------------------------------------------------------------------------------------------------
	v_column_exists := 0;
	v_table_exists := 0;
	v_seq_exists := 0;
	v_index_exists := 0;
	
	-- ----------------------------------------------------------------------------------------------------------
	-- Taula TM_ROLE
	SELECT  COUNT(1)
	INTO v_table_exists
	FROM user_tables
	WHERE table_name = 'TM_ROLE';
	IF (v_table_exists = 0) THEN
	
		EXECUTE IMMEDIATE '	CREATE TABLE TM_ROLE(
							"ROLE_IDPK" NUMBER NOT NULL,  
							"ROLE_NAME" VARCHAR2(50 BYTE) NOT NULL,
							"CREATED_BY" VARCHAR2(50 BYTE), 
							"CREATION_DATE" DATE,
							"UPDATED_BY" VARCHAR2(50 BYTE), 
							"UPDATE_DATE" DATE,
							CONSTRAINT "PK_TM_ROLE" PRIMARY KEY ("ROLE_IDPK"))';
		
	END IF;	
	
	-- SEQ SEQ_TM_ROLE
	SELECT COUNT(1)
	INTO v_seq_exists
	FROM user_sequences
	WHERE sequence_name = 'SEQ_TM_ROLE';
	IF (v_seq_exists = 0) THEN
		EXECUTE IMMEDIATE 'CREATE SEQUENCE  SEQ_TM_ROLE  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE';
	END IF;
	
	-- TRIGGER TR_TM_ROLE_BIU
	EXECUTE IMMEDIATE '	CREATE OR REPLACE TRIGGER TR_TM_ROLE_BIU 
						BEFORE INSERT OR UPDATE ON TM_ROLE
						REFERENCING NEW AS NEW OLD AS OLD FOR EACH row
						BEGIN
							IF (INSERTING) THEN
								:NEW.ROLE_IDPK 		:= SEQ_TM_ROLE.NEXTVAL;
								:NEW.CREATED_BY 	:= FN_WHOIAM;
								:NEW.CREATION_DATE  := SYSDATE;
							ELSE
								:NEW.UPDATED_BY 	:= FN_WHOIAM;
								:NEW.UPDATE_DATE  	:= SYSDATE;
							END IF;
						END TR_TM_ROLE_BIU;';
	
	-- ENABLE TRIGGER TR_TM_ROLE_BIU
	EXECUTE IMMEDIATE '	ALTER TRIGGER TR_TM_ROLE_BIU ENABLE';
    
	-- ----------------------------------------------------------------------------------------------------------
	--Taula TB_USER
	SELECT  COUNT(1)
	INTO v_table_exists
	FROM user_tables
	WHERE table_name = 'TB_USER';
	IF (v_table_exists = 0) THEN
	
		EXECUTE IMMEDIATE '	CREATE TABLE TB_USER(
							"USER_IDPK" NUMBER NOT NULL, 
							"USERNAME" VARCHAR2(50 BYTE) NOT NULL,
							"PASSWORD" VARCHAR2(200 BYTE) NOT NULL,
							"EMAIL" VARCHAR2(100 BYTE),
							"STRIKES" NUMBER,
							"ENABLED" NUMBER(1,0),
							"CREATED_BY" VARCHAR2(50 BYTE), 
							"CREATION_DATE" DATE,
							"UPDATED_BY" VARCHAR2(50 BYTE), 
							"UPDATE_DATE" DATE,
							CONSTRAINT "PK_TB_USER" PRIMARY KEY ("USER_IDPK"))';
		
	END IF;	
	
	-- SEQ SEQ_TB_USER
	SELECT COUNT(1)
	INTO v_seq_exists
	FROM user_sequences
	WHERE sequence_name = 'SEQ_TB_USER';

	IF (v_seq_exists = 0) THEN
		EXECUTE IMMEDIATE 'CREATE SEQUENCE  SEQ_TB_USER  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE';
	END IF;
	
	-- UN1 UN1_TB_USER
	SELECT COUNT(1)
	INTO v_index_exists
	from all_indexes
	where owner = 'SYS_ECHO'
	and index_name = 'UN1_TB_USER';
	IF (v_index_exists = 0) THEN
		EXECUTE IMMEDIATE 'CREATE UNIQUE INDEX UN1_TB_USER ON TB_USER(USERNAME)';
		EXECUTE IMMEDIATE 'CREATE UNIQUE INDEX UN2_TB_USER ON TB_USER(EMAIL)';
	END IF;
	
    -- ----------------------------------------------------------------------------------------------------------
	--Taula IT_USER_ROLE
	SELECT  COUNT(1)
	INTO v_table_exists
	FROM user_tables
	WHERE table_name = 'IT_USER_ROLE';
	IF (v_table_exists = 0) THEN
	
		EXECUTE IMMEDIATE '	CREATE TABLE IT_USER_ROLE(
                            "USER_ID" NUMBER NOT NULL, 
							"ROLE_ID" NUMBER NOT NULL,
							PRIMARY KEY ("USER_ID", "ROLE_ID"),
                            CONSTRAINT "FK1_TB_USER" FOREIGN KEY ("ROLE_ID") REFERENCES TM_ROLE("ROLE_IDPK"),
                            CONSTRAINT "FK2_TB_USER" FOREIGN KEY ("USER_ID") REFERENCES TB_USER("USER_IDPK"))';
		
	END IF;	
    
    -- ----------------------------------------------------------------------------------------------------------
	-- TRIGGER TR_TB_USER_BIU
	EXECUTE IMMEDIATE '	create or replace TRIGGER TR_TB_USER_BIU 
						BEFORE INSERT OR UPDATE ON TB_USER
						REFERENCING NEW AS NEW OLD AS OLD FOR EACH row
						BEGIN
							IF (INSERTING) THEN
								:NEW.USER_IDPK := SEQ_TB_USER.NEXTVAL;
								:NEW.STRIKES := 0;
								:NEW.ENABLED := 1;
								:NEW.CREATED_BY := FN_WHOIAM;
								:NEW.CREATION_DATE  := SYSDATE;
							ELSE
								:NEW.UPDATED_BY := FN_WHOIAM;
								:NEW.UPDATE_DATE  := SYSDATE;
							END IF;
						END TR_TB_USER_BIU;';
                        
    -- TRIGGER TR_TB_USER_AIU
	EXECUTE IMMEDIATE '	create or replace TRIGGER TR_TB_USER_AI
						AFTER INSERT ON TB_USER
						REFERENCING NEW AS NEW OLD AS OLD FOR EACH row
                        DECLARE
                            V_ROLE_ID number;
                            V_USER_ID number;
						BEGIN
                            SELECT ROLE_IDPK INTO V_ROLE_ID  FROM TM_ROLE WHERE ROLE_NAME = ''ROLE_USER'';
                            V_USER_ID := :NEW.USER_IDPK;
                            
                            -- SET ROLE_USER as default  role
                            INSERT INTO IT_USER_ROLE ( USER_ID, ROLE_ID )
                            VALUES ( V_USER_ID, V_ROLE_ID );
						END TR_TB_USER_BIU;';
                        
    -- ENABLE TRIGGER TR_TB_USER_BIU
	EXECUTE IMMEDIATE '	ALTER TRIGGER TR_TB_USER_BIU    ENABLE';
                        
	-- ----------------------------------------------------------------------------------------------------------	
	--Taula TB_LOG
	SELECT  COUNT(1)
	INTO v_table_exists
	FROM user_tables
	WHERE table_name = 'TB_LOG';
	IF (v_table_exists = 0) THEN
	
		EXECUTE IMMEDIATE '	CREATE TABLE TB_LOG(
							"LOG_IDPK"  NUMBER NOT NULL, 
							"LOCATION"  VARCHAR2(250 BYTE), 
							"MESSAGE"   VARCHAR2(4000 BYTE),
							"TYPE"      VARCHAR2(10 BYTE),
							"CREATION_DATE" DATE,
							CONSTRAINT "PK_TB_LOG" PRIMARY KEY ("LOG_IDPK"))';
    
	END IF;	
	
	-- SEQ SEQ_TB_LOG
	SELECT COUNT(1)
	INTO v_seq_exists
	FROM user_sequences
	WHERE sequence_name = 'SEQ_TB_LOG';
	IF (v_seq_exists = 0) THEN
		EXECUTE IMMEDIATE 'CREATE SEQUENCE  SEQ_TB_LOG  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE';
	END IF;
	
	-- TRIGGER TR_TB_LOG_BIU
	EXECUTE IMMEDIATE '	CREATE OR REPLACE TRIGGER TR_TB_LOG_BIU 
						BEFORE INSERT OR UPDATE ON TB_LOG
						REFERENCING NEW AS NEW OLD AS OLD FOR EACH row
						BEGIN
							IF (INSERTING) THEN
								:NEW.LOG_IDPK 			:= SEQ_TB_LOG.NEXTVAL;
								:NEW.CREATION_DATE  := SYSDATE;
							END IF;
						END TR_TB_LOG_BIU;';
	

	-- ENABLE TRIGGER TR_TB_LOG_BIU
	EXECUTE IMMEDIATE '	ALTER TRIGGER TR_TB_LOG_BIU ENABLE';
	
    -- ----------------------------------------------------------------------------------------------------------

EXCEPTION WHEN OTHERS THEN
	v_errm := SUBSTR('WARNING: SYS_ECHO - ' || SQLERRM, 1 , 200);
	DBMS_OUTPUT.PUT_LINE(v_errm);
END;		
/
