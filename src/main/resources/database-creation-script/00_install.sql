set sqlbl on;
set define off;										 
prompt ===========================                    
prompt 01_ddl                                         
@@01_ddl.sql;                                                                      
prompt ===========================                    
prompt 02_dml                                         
@@02_dml.sql;                                                                            
prompt ===========================                    
prompt Compilacio esquema SYS_ECHO: inici                 
EXEC dbms_utility.compile_schema('SYS_ECHO');             
prompt ===========================                    
prompt Script finalitzat                              
/ 