SET DEFINE OFF;
    --Nous INSERT
    -- INSERT TM_ROL
    Insert into TM_ROLE (ROLE_NAME) values ('ROLE_USER');
    Insert into TM_ROLE (ROLE_NAME) values ('ROLE_MODERATOR');
    Insert into TM_ROLE (ROLE_NAME) values ('ROLE_ADMIN');
	/
    
    -- INSERT TB_USER
	Insert into TB_USER (USERNAME,PASSWORD) values ('user','$2a$12$SqdqykVgTJjFh7Nw0lIWVeLShhynqAAYmEmB/3IpliqPnznFsf9bi');
    Insert into TB_USER (USERNAME,PASSWORD) values ('admin','$2a$12$SqdqykVgTJjFh7Nw0lIWVeLShhynqAAYmEmB/3IpliqPnznFsf9bi');
	/

	
COMMIT;